<?php

namespace Spip\Bigup;

/**
 * Retours de la classe Flow
 * Indique le code de réponse http, et d’éventuelles données.
 */
class FlowResponse
{
	public function __construct(
		public $code,
		public $data = null
	) {
	}
}
